# Copyright (c) 2016-2025 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later

# Expand aliases in non-interactive shells
shopt -s expand_aliases


# = Convenience exports ====================================================== #

# Some programs will assume this default
export EDITOR="nvim"

# Set default PGP key
export GPGKEY="03642DC21A31C31F61C75CD6C38D640A4906B16B"

# Workaround to gnupg issue T6873
export PASSWORD_STORE_GPG_OPTS="--default-key local@key"

# Colours
export BLACK='\e[30m'
export RED='\e[31m'
export GREEN='\e[32m'
export YELLOW='\e[33m'
export BLUE='\e[34m'
export PURPLE='\e[35m'
export CYAN='\e[36m'
export WHITE='\e[37m'
export ON_BLACK='\e[40m'
export ON_RED='\e[41m'
export ON_GREEN='\e[42m'
export ON_YELLOW='\e[43m'
export ON_BLUE='\e[44m'
export ON_PURPLE='\e[45m'
export ON_CYAN='\e[46m'
export ON_WHITE='\e[47m'
export BOLD='\e[1m'		# Bold
export DIM='\e[2m'		# Dim
export EMPH='\e[3m'		# Italicize
export UND='\e[4m'		# Underline
export LIGHT='\e[7m'		# Highlight
export NC='\e[m'		# No colour / colour reset

# LESS man page colours (makes Man pages more readable).
export LESS_TERMCAP_mb=$'\e[01;34m'
export LESS_TERMCAP_md=$'\e[01;34m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;41;37m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[01;32m'


# = Aliases ================================================================== #

# Vim aliases
alias vim='nvim'
alias vi='nvim'
alias vimdiff='nvim -d'
if [ "$(id -u)" == "0" ]; then
	# Disable shada and user config
	alias vim='nvim -i NONE -u NONE'
fi
# Disable swap, shada and user config
alias vimprivate='nvim -n -i NONE -u NONE'
export SAFEEDITOR='nvim -n -i NONE -u NONE'

# force confirmation on delete, move and copy
alias rm='rm -ri'
alias cp='cp -i'
alias mv='mv -i'

alias ..='cd ..'
alias grep='grep --color'
alias diff='diff -u'
alias less='less -S'

# 'ls' and 'ls' like commands
alias ls='ls -h --color'
alias ll="ls -lv --group-directories-first"
alias la='ll -A'
alias tree='tree -Csuh'

# Directory inspection
alias du='du -kh'
alias df='df -kTh'

alias rg='rg --hidden --ignore-file ~/.gitignore --ignore-file ~/.rgignore'

# Don't leak information through the editor.
alias pass='EDITOR=$SAFEEDITOR pass'

alias pastebin="nc termbin.com 9999"

alias audio-loopback="gst-launch-1.0 -v alsasrc buffer-time=35000 !  pulsesink"


# - FZF ---------------------------------------------------------------------- #

if [ -f /usr/share/fzf/key-bindings.bash ]; then
	source /usr/share/fzf/key-bindings.bash
fi
if [ -f /usr/share/fzf/completion.bash ]; then
	source /usr/share/fzf/completion.bash
fi

export FZF_DEFAULT_OPTS='--height 40% --layout=reverse --info=inline --no-mouse --multi'
export FZF_COMPLETION_OPTS=$FZF_DEFAULT_OPTS
export FZF_COMPLETION_TRIGGER='**'

complete -o default -o bashdefault -F _fzf_path_completion ll
complete -o default -o bashdefault -F _fzf_path_completion la
complete -o default -o bashdefault -F _fzf_path_completion tree
