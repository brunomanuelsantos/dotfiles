#!/usr/bin/env python
# Copyright (c) 2019 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later


"""
Unpack a MIME message into a directory of files and launch a browser with each
HTML part found.
"""

import os
import re
import sys

import email
import mimetypes
import webbrowser

def decode(outdir):
    msg = email.message_from_string(sys.stdin.read())

    html_parts = []

    counter = 1
    for part in msg.walk():
        if part.get_content_maintype() == 'multipart':
            continue

        filename = part.get_filename()
        ext = mimetypes.guess_extension(part.get_content_type())

        if not filename:
            if not ext:
                # Fallback to a 'binary' file.
                ext = '.bin'
            filename = 'part-%03d%s' % (counter, ext)

        # Remove content ID markings from the source in hopes that that will be
        # enough to render this thing. This works for a bunch of email I have,
        # but is it complete?
        if ext == '.htm' or ext == '.html':
            part.set_payload(re.sub(bytes(r'"cid:(.+?)@.+?"', 'utf-8'),
                                    bytes(r'"\1"', 'utf-8'),
                                    part.get_payload(decode=True)))
            html_parts.append(os.path.join(outdir, filename))

        counter += 1
        fp = open(os.path.join(outdir, filename), 'wb')
        fp.write(part.get_payload(decode=True))
        fp.close()

    return html_parts

if __name__ == '__main__':
    try:
        outdir = sys.argv[1]
        os.makedirs(outdir)
    except (OSError, IndexError):
        raise

    for f in decode(outdir):
        webbrowser.open(f)
