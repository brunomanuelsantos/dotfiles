# Copyright (c) 2017-2023 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later

PATH=$HOME/.bin:$PATH

if [[ -f ~/.bashrc ]]; then
    . ~/.bashrc
fi
