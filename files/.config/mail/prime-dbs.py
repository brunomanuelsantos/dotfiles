#!/usr/bin/python
"""
Notmuch and bogofilter primer
=============================

Read me or get burnt!

This will take two small eternities for anything other than small to modest mail
collections. This comprises 2 steps:

1. Call `notmuch new`. You can do this manually before and still call this
   script afterwards (it only takes forever the 1st time anyway). It can be
   interrupted without loss of progress.

   This step is done here for convenience, mostly to guarantee that notmuch's
   database is synchronized before doing anything else.

2. Train bogofilter with all existing mail. This is _not_ interruptible. Or
   rather, it is, but with potentially heavy consequences. The rest of this
   documentation focuses on this 2nd step.


The spam training data is primed with an initial filtering step that adds the
tag `spam` to spam email. Typically this filter should add this tag to any mail
currently residing in spam folders. All other mail is considered good mail and
will also be used for training.

Attention::
    Spam training is not interruptible and generally speaking you'll want to
    stop all scheduled operations in your mail repository. This includes IMAP
    synchronization, and any other operation involving bogofilter.

If the spam training is interrupted after the 1st commit to the bogofilter
database is done, you probably should delete the current bogofilter database and
start over. If you don't do that, it will still take just as long to complete,
but emails that were already counted will be counted again. This can skew the
statistics and mess up with spam detection early on, which is what we want to
solve with the priming to begin with.

Note::
    Any skew introduced by counting certain messages twice can remain noticeable
    for a long time depending on size of email collection and your luck as to
    which messages were counted multiple times.

With that said, the spam training is entirely optional! In fact, not doing it at
all will likely be better than doing it wrong. Using every single historical email
as a seed essentially pedantry.

If you don't want to use past email for training at all, remove the `new` tag
from it and don't run this script. To save time, use a temporary notmuch
configuration in which no `new` flag is added before running `notmuch new` for
the 1st time.
"""

import logging
import mailfilter
import notmuch
from progress.bar import Bar as ProgressBar
import subprocess
import time

# Filters to prime the notmuch database with spam tags based on mail location.
# Since we'll be processing all files, we can remove the `new` flag right away.
_notmuch_primer = [
    ['folder:tecnico/Junk',
     False,
     ['spam'],
     []],
    ['folder:gmail/Spam',
     False,
     ['spam'],
     []],
    ['folder:ampyx/Junk',
     False,
     ['spam'],
     []],
    ['tag:new',
     False,
     [],
     ['new']],
]

if __name__ == '__main__':
    logging.basicConfig(level=logging.WARNING)

    subprocess.run(['notmuch', 'new'])

    for rule in _notmuch_primer:
        progress = ProgressBar('Filtering')
        mailfilter.apply_filter(*rule, None, progress)

    mailfilter.train_spam('not tag:spam', False, False, ProgressBar('Training ham'))
    mailfilter.train_spam('tag:spam', True, False, ProgressBar('Training spam'))
