#!/usr/bin/env python
"""
Mail filtering tool
===================

This uses notmuch mostly for its database, but it was designed to integrate in
a folder email setup rather than a tag based one. With that said, if the
filters are configured _not_ to move email files, then the result is the same.

Note however that due to how notmuch indexes files, if the same email exists in
multiple mailboxes, behaviour may not always be the intended one. In
particular, moving filters will likely do the wrong thing or result in errors.

Note::
    Moving filters require a maildir format as the email storage backend.

This script handles spam completely autonomously and independent of the MUA:
1. The automatic tagging on arrival counts all ham and unsure messages as good
   and all spam as bad.
2. An additional spam tagging is done on all messages that were moved to the
   spam folders.
"""

import logging
import notmuch
import os
import re
import subprocess
import time

_mail_root = os.path.expanduser('~/.mail')

# Filters are applied in the order they are specified in!
_pre_tagging_filters = {
    # This is a reasonably well vetted list pre sorted in the server and I'm
    # not combing it nearly well enough to know whether the tagging is having a
    # net positive result. On the other hand, moving all the false positives
    # back to their place is annoying. Just make this list invisible to the
    # tagging process.
    'ignore lkml': {
        'search': 'tag:new and folder:/tecnico/Lists/linux-kernel/',
        'moveto': False,
        'atags': [],
        'rtags': ['new'],
    },
}

_post_tagging_filters = {
    # Move tagged spam. Relies on a previous tagging step.
    'move tecnico spam': {
        'search': 'tag:new and tag:spam and folder:/tecnico/',
        'moveto': 'tecnico/Junk',
        'atags': [],
        'rtags': [],
    },
    'move gmail spam': {
        'search': 'tag:new and tag:spam and folder:/gmail/',
        'moveto': 'gmail/Spam',
        'atags': [],
        'rtags': [],
    },
    'move spinworks spam': {
        'search': 'tag:new and tag:spam and folder:/spinworks/',
        'moveto': 'spinworks/spam',
        'atags': [],
        'rtags': [],
    },

    # The user may move files manually into/out of the spam folders. Instead of
    # relying on complicated rules in the MUA to call bogofilter, we'll detect
    # these moves and make sure to do the right thing here. We do this with a
    # temporary flag with which we can call `train_spam` afterwards. This tag
    # should be cleared afterwards in the last filters.
    'manual spam': {
        'search': 'not tag:spam and (folder:tecnico/Junk or folder:gmail/Spam or folder:spinworks/spam)',  # noqa: E501
        'moveto': False,
        'atags': ['spam', '_untrained'],
        'rtags': [],
    },
    'manual ham': {
        'search': 'tag:spam and not folder:tecnico/Junk and not folder:gmail/Spam and not folder:spinworks/spam',  # noqa: E501
        'moveto': False,
        'atags': ['_untrained'],
        'rtags': ['spam'],
    },
}

# Last filters. These are meant for clean-up purposes only.
_clean_up_filters = {
    # Delete emails from high volume mailing lists that are too old provided
    # they're not flagged, but ignoring if they're marked unread: face it,
    # you're not reading them.
    'trim high volume': {
        'search': 'folder:tecnico/Lists/linux-kernel and not tag:flagged and date:..3months',  # noqa: E501
        'moveto': None,
        'atags': [],
        'rtags': [],
    },

    # Remove temporary tags used by this script.
    'clean-up temporary tags': {
        'search': 'tag:_untrained',
        'moveto': False,
        'atags': [],
        'rtags': ['_untrained'],
    },

    # Remove the new tags. From this point onwards we can no longer distinguish
    # which messages have just arrived. This should probably be the last one.
    'clean-up new tag': {
        'search': 'tag:new',
        'moveto': False,
        'atags': [],
        'rtags': ['new'],
    },
}


def _move_msg(db, msg, maildir):
    """
    Move a mail file safely to a different maildir.

    Args:
        msg: Message to be moved.
        maildir: Mailbox to move the file to. If `None`, the email is deleted;
                 if `False` nothing is done.
    """
    msg_id = msg.get_message_id()
    src = msg.get_filename()

    if maildir is False:
        return

    if maildir is None:
        try:
            os.remove(src)
        except FileNotFoundError:
            logging.error('Message {} does not exist'.format(msg_id))
            return
        logging.debug('Deleted {}'.format(msg_id))

    else:
        src_maildir = os.path.dirname(os.path.dirname(src))
        src_maildir = os.path.relpath(src_maildir, _mail_root)
        if src_maildir == maildir:
            return

        # Get rid of the UID (U=number) in the name, but keep all the flags.
        name = re.sub(r'(,U=\d*)?(:.*)', r'\g<2>', os.path.basename(src))
        dst = os.path.join(_mail_root, maildir, 'cur', name)

        try:
            # Move atomically.
            os.replace(src, dst)
        except (OSError, FileNotFoundError):
            s = 'Renaming {} failed; make sure database is in sync and that {} exists'  # noqa: E501
            logging.error(s.format(msg_id, maildir))
            return

        new_msg, status = db.index_file(dst)
        new_msg_id = new_msg.get_message_id()
        logging.debug('Moved {} to {}'.format(new_msg_id, maildir))

    # The original file was either deleted or renamed.
    db.remove_message(src)


def apply_filter(search, moveto, atags, rtags, name=None, progress=None):
    """
    Bulk message tag.

    This can present a progress bar, but the total count is based on an
    estimate.

    Args:
        search: Notmuch search term.
        moveto: Maildir to move the email to; may be None to delete and False
            to ignore.
        atags: Flags to be added.
        rtags: Flags to be removed.
        name: Filter name for log purposes.
        progress: `progress.bar` compatible progress reporter object.
    """
    t_start = time.time()
    db = notmuch.Database(mode=notmuch.Database.MODE.READ_WRITE)

    query = notmuch.Query(db, search)
    if progress:
        progress.index = 0
        progress.max = query.count_messages()

    n_msgs = 0
    for msg in query.search_messages():
        msg.freeze()
        for t in atags:
            msg.add_tag(t)
        for t in rtags:
            msg.remove_tag(t)
        msg.thaw()
        msg.tags_to_maildir_flags()

        _move_msg(db, msg, moveto)

        n_msgs += 1
        if progress:
            progress.next()

    if db.needs_upgrade():
        db.upgrade()

    if progress:
        progress.finish()

    t_delta = time.time() - t_start
    name = name + ": " if name is not None else ""
    logging.info(f'{name}Filtered {n_msgs} messages in {t_delta:1.2f} seconds')


def autotag_spam(search, train=True, progress=None):
    """
    Automatically tag spam mail with bogofilter.

    By default, this will register the matched mail in bogofilter's database,
    which is typically desired only for _new_ mail. If the assessment is wrong,
    the database will need to be adjusted later on to avoid skewing of future
    evaluations.

    This can present a progress bar, but the total count is based on an
    estimate.

    Args:
        search: Notmuch search term.
        train: Whether to register the emails in the bogofilter's database.
        progress: `progress.bar` compatible progress reporter object.
    """
    t_start = time.time()
    db = notmuch.Database(mode=notmuch.Database.MODE.READ_WRITE)
    query = notmuch.Query(db, search)

    if progress:
        progress.index = 0
        progress.max = query.count_messages()

    n_msgs = 0
    n_spam = 0
    opts = '-uI' if train else '-I'
    for msg in query.search_messages():
        msg_id = msg.get_message_id()
        bf = subprocess.run(['bogofilter', opts, msg.get_filename()])

        if bf.returncode == 0:
            logging.debug('Message {} marked as spam'.format(msg_id))
            msg.add_tag('spam')
            n_spam += 1

        elif bf.returncode == 1:
            logging.debug('Message {} marked as ham'.format(msg_id))
            msg.remove_tag('spam')

        elif bf.returncode == 2:
            # If unsure, the message wasn't registered. Register it as ham.
            opts = '-nI' if train else '-I'
            bf = subprocess.run(['bogofilter', opts, msg.get_filename()])
            logging.debug('Message {} marked as ham'.format(msg_id))
            msg.remove_tag('spam')

        else:
            logging.error('Message {} skipped due to I/O error'.format(msg_id))

        n_msgs += 1
        if progress:
            progress.next()

    if progress:
        progress.finish()

    t_delta = time.time() - t_start
    s = 'Tagged {} of {} messages as spam in {:1.2f} seconds'
    logging.info(s.format(n_spam, n_msgs, t_delta))


def train_spam(search, spam, override=False, progress=None):
    """
    Train spam setting any matching email as spam or not as per `spam`'s value.

    This can present a progress bar, but the total count is based on an
    estimate.

    Args:
        search: Notmuch search term.
        spam: Whether to mark the matching mail as spam or not spam.
        override: Whether the message was registered before and must be
                  unregistered to avoid double counting.
        progress: `progress.bar` compatible progress reporter object.
    """
    t_start = time.time()
    db = notmuch.Database(mode=notmuch.Database.MODE.READ_WRITE)

    opts = '-'
    opts += ('N' if spam else 'S') if override else ''
    opts += 's' if spam else 'n'
    opts += 'I'

    query = notmuch.Query(db, search)

    if progress:
        progress.index = 0
        progress.max = query.count_messages()

    n_msg = 0
    for msg in query.search_messages():
        bf = subprocess.run(['bogofilter', opts, msg.get_filename()])
        if bf.returncode == 3:
            s = 'Message {} skipped due to I/O error'
            logging.error(s.format(msg.get_message_id()))
        n_msg += 1
        if progress:
            progress.next()
    if progress:
        progress.finish()

    t_delta = time.time() - t_start
    s = 'Trained {} with {} messages in {:1.2f} seconds'
    logging.info(s.format('spam' if spam else 'ham', n_msg, t_delta))


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    subprocess.run(['notmuch', 'new'])

    for name, rule in _pre_tagging_filters.items():
        apply_filter(**rule, name=name)

    autotag_spam('tag:new')

    for name, rule in _post_tagging_filters.items():
        apply_filter(**rule, name=name)

    train_spam('not tag:spam and tag:_untrained', False, override=True)
    train_spam('tag:spam and tag:_untrained', True, override=True)

    for name, rule in _clean_up_filters.items():
        apply_filter(**rule, name=name)
