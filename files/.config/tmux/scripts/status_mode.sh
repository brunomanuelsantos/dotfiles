#!/usr/bin/env bash
# Copyright (c) 2023 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later

declare -r mode_prompt_tag="\#{mode_prompt}"
declare -r mode_style_tag="\#{mode_style}"

tmux_option() {
  tmux show-option -gqv "$1"
}

init_status_mode() {

  local -r empty_prompt="TMUX"
  local -r prefix_prompt="WAIT"
  local -r copy_prompt="COPY"
  local -r sync_prompt="SYNC"

  # Colours should match those of vim's modes.
  local -r empty_style="#[fg=black bold]#[bg=blue]"
  local -r prefix_style="#[fg=black bold]#[bg=green]"
  local -r copy_style="#[fg=black bold]#[bg=magenta]"
  local -r sync_style="#[fg=black bold]#[bg=orange]"

  local -r mode_prompt="#{?client_prefix,$prefix_prompt,#{?pane_in_mode,$copy_prompt,#{?pane_synchronized,$sync_prompt,$empty_prompt}}}"
  local -r mode_style="#{?client_prefix,$prefix_style,#{?pane_in_mode,$copy_style,#{?pane_synchronized,$sync_style,$empty_style}}}"

  local -r status_left="$(tmux_option "status-left")"
  local -r status_left_with_prompt="${status_left/$mode_prompt_tag/$mode_prompt}"
  tmux set-option -gq "status-left" "${status_left_with_prompt/$mode_style_tag/$mode_style}"

  local -r status_right="$(tmux_option "status-right")"
  local -r status_right_with_prompt="${status_right/$mode_prompt_tag/$mode_prompt}"
  tmux set-option -gq "status-right" "${status_right_with_prompt/$mode_style_tag/$mode_style}"
}

init_status_mode
