#!/usr/bin/env bash
# Copyright (c) 2023 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later

NAME=$1

get_name() {
  case $NAME in

  # Office.
  tmux)    echo "  tmux";;
  neomutt) echo "󰇮  mail";;
  weechat) echo "󰭻  irc";;
  ranger)  echo "  $NAME";;
  ssh)     echo "  $NAME";;
  htop)    echo "  $NAME";;
  top)     echo "  $NAME";;

  # Dev tools.
  vi)      echo "  vim";;
  vim)     echo "  vim";;
  nvim)    echo "  vim";;
  vimdiff) echo "  vim";;
  vimdot)  echo "  vim";;
  git)     echo "  $NAME";;
  make)    echo "  $NAME";;
  meson)   echo "  $NAME";;
  ninja)   echo "  $NAME";;
  cmake)   echo "  $NAME";;
  man)     echo "  $NAME";;
  cppman)  echo "  $NAME";;
  less)    echo "󰗚  $NAME";;
  more)    echo "󰗚  $NAME";;
  grep)    echo "  $NAME";;
  rg)      echo "  $NAME";;
  fzf)     echo "  $NAME";;

  # Interpreters.
  bash)    echo "  $NAME";;
  dash)    echo "  $NAME";;
  fish)    echo "  $NAME";;
  zsh)     echo "  $NAME";;
  tcsh)    echo "  $NAME";;
  python)  echo "  $NAME";;
  python2) echo "  $NAME";;
  python3) echo "  $NAME";;
  lua)     echo "  $NAME";;

  # If all else fails...
  *) echo "  $NAME";;

  esac
}

get_name
