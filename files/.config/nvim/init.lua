-- Copyright (c) 2016-2024 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- = First things first ===================================================== --

-- Map leader.
vim.g.mapleader = [[\]]
vim.g.maplocalleader = [[\]]

-- Me, me and me.
vim.g.author = 'Bruno Santos'
vim.g.email = 'brunomanuelsantos@tecnico.ulisboa.pt'

-- = Plugins ================================================================ --

require('my-plugins.plugins')

-- = General ================================================================ --

-- Load shell aliases.
vim.env.BASH_ENV = '~/.bash_aliases'

-- EOL options to try while auto-detecting.
vim.o.fileformats = 'unix,dos,mac'

-- Disable modelines.
vim.o.modeline = false

-- Timeout value in ms.
vim.o.timeout = true
vim.o.timeoutlen = 1000
-- vim.o.ttimeoutlen = 0 -- why was this set?

-- - Terminal --------------------------------------------------------------- --

-- Exit terminal mode with escape. Must not be <Esc> only without an alternate
-- way of sending an <Esc> to the terminal in case (e.g.) we're trying to go to
-- normal mode in a nested vim instance.
vim.keymap.set('t', '<M-Esc>',  [[<C-\><C-n>]], { silent = true, desc = 'Exit terminal mode' })

-- Disable superfluous stuff in terminals. This is a special case because in
-- this case we cannot override settings with file type plugins.
vim.api.nvim_create_autocmd({ 'TermOpen' }, {
  callback = function()
    vim.wo.spell = false
    vim.wo.number = false
    vim.wo.foldcolumn = '0'
    vim.wo.signcolumn = 'no'
    vim.cmd('startinsert')
  end,
})
vim.api.nvim_create_autocmd({ 'TermClose' }, {
  callback = function()
    vim.wo.spell = true
    vim.wo.number = true
    vim.wo.foldcolumn = '1'
    vim.wo.signcolumn = 'yes'
  end,
})

-- - File ignore ------------------------------------------------------------ --

-- Many _typically_ binary files should just be hidden. But every now and then
-- these are either not binary at all or not even files. And even when they are,
-- there may still be some interest in opening them in Vim. Adding them to
-- wildignore makes finding these harder than it should be, so most of them
-- really belong in suffixes.

-- Assortment of binary files.
vim.o.suffixes = ''
  .. '*.pdf,*.odt,*.odp,*.ods,*.doc*,*.ppt*,*.xls*,'
  .. '*.jpg,*.png,*.bmp,*.svg,*.gif'
-- Archive files.
vim.o.suffixes = vim.o.suffixes .. '*.tar.*,*.zip,*.7z,*.gz'
-- Object files.
vim.o.suffixes = vim.o.suffixes .. '*.o,*.obj,*.so,*.a,a.out,*.elf,*.bin,*.exe'
-- Backups.
vim.o.suffixes = vim.o.suffixes .. '*.old,*.bak'
-- LaTeX generated files.
vim.o.suffixes = vim.o.suffixes
  .. '*.acn,*.aux,*.acr,*.alg,*.fdb_latexmk,*.fls,*.glo,*.gls,*.glg,*.toc,'
  .. '*.blg,*.bbl,*.ist,*.out,*.syg,*.syi,*.slg,*.glsdefs,*.bcf,*.run.xml'

-- Some files/folders are so uncommonly wanted though that we'd better not
-- search in them for performance reasons, if nothing else.

-- Version control files.
vim.o.wildignore = '.git,.hg,.bzr,.svn'
-- Editing helper files.
vim.o.wildignore = vim.o.wildignore .. 'Session.vim,*.swp,.tags'
-- Python generated files.
vim.o.wildignore = vim.o.wildignore .. '__pycache__'
-- Qt generated files.
vim.o.wildignore = vim.o.wildignore .. '*.moc.o,*.moc.cpp'

-- = Interface ============================================================== --

-- Pest control.
vim.o.mouse = ''

-- Disable welcome screen.
vim.o.shortmess = vim.o.shortmess .. 'I'

-- 24bit colour support. What a time to be alive!
vim.o.termguicolors = true

-- Show command as it's being entered.
vim.o.showcmd = true

-- Hide buffers instead of closing them.
vim.o.hidden = true

-- - Theme ------------------------------------------------------------------ --

vim.api.nvim_create_user_command('DebugHighlightGroups', function()
  vim.cmd(':so $VIMRUNTIME/syntax/hitest.vim')
end, { desc = 'Debug all active highlight groups' })

-- Make the vertical window borders less conspicuous.
vim.api.nvim_set_hl(0, 'WinSeparator', { fg = '#414037' })

-- Search highlights.
vim.api.nvim_set_hl(0, 'IncSearch', { fg = '#383830', bg = '#fd971f', bold = true })
vim.api.nvim_set_hl(0, 'Search',    { fg = '#fd971f', bg = '#383830' })
vim.api.nvim_set_hl(0, 'CurSearch', { fg = '#fd971f', bg = '#383830' })

-- Parenthesis match highlighting.
vim.api.nvim_set_hl(0, 'MatchParen', {
  fg = '#cc6633',
  bg = '#383830',
  bold = true,
  underline = true,
})

-- Make diagnostics' borders visible.
vim.api.nvim_set_hl(0, 'FloatBorder', { fg = '#f8f8f2' })

-- Less conspicuous white space.
vim.api.nvim_set_hl(0, 'Whitespace', { fg = '#505040' })
vim.api.nvim_set_hl(0, 'Nontext', { fg = '#505040' })

-- - Layout ----------------------------------------------------------------- --

-- Always show status and tab bar.
vim.o.laststatus = 2
vim.o.showtabline = 2

-- Show line numbers.
vim.o.number = true
vim.o.numberwidth = 1

-- Put vertical/horizontal splits on the right/bottom.
vim.o.splitright = true
vim.o.splitbelow = true

-- Keep signcolumn on at all times.
vim.o.signcolumn = 'yes'

-- - Wrapping --------------------------------------------------------------- --

-- Indent wrapped lines and try to wrap at word boundaries.
vim.o.wrap = true
vim.o.breakindent = true
vim.o.linebreak = true

-- - Diagnostics ------------------------------------------------------------ --

vim.diagnostic.config({
  -- Disable inline messages and underlines which are really annoying.
  virtual_text = false,
  underline = false,
  -- Show gutter signs.
  signs = true,
  -- Update on insert.
  update_in_insert = true,
  -- Add a border to the diagnostics' windows.
  float = { border = 'single' }
})

-- Better gutter symbols.
local signs = { Error = ' ', Warn = ' ', Hint = '󰌶 ', Info = '󰙎 ' }
for type, icon in pairs(signs) do
  local hl = 'DiagnosticSign' .. type
  vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
end

vim.keymap.set('n', '<leader>e',  vim.diagnostic.open_float, { silent = true, desc = 'Open floating diagnostic message' })
vim.keymap.set('n', '<leader>dd', vim.diagnostic.setloclist, { silent = true, desc = 'Open diagnostics list' })
vim.keymap.set('n', '[d',         vim.diagnostic.goto_prev,  { silent = true, desc = 'Go to previous diagnostic message' })
vim.keymap.set('n', ']d',         vim.diagnostic.goto_next,  { silent = true, desc = 'Go to next diagnostic message' })

-- = Files, backups, undo, etc. ============================================= --

-- Some of these options may pose security issues creating side channels for
-- information leakage. However, these are the *right defaults*. Setting up an
-- alias like 'vimprivate' disabling some of these options to edit sensitive
-- files is highly recommended.

-- Set to auto read when a file is changed.
vim.o.autoread = true

-- Safe write to disk without the stray files.
vim.o.backup = false
vim.o.writebackup = true

-- - Swap & undo ------------------------------------------------------------ --

-- Enable swap.
vim.o.swapfile = true

-- ms and character count to trigger swap file save.
vim.o.updatetime = 500
vim.o.updatecount = 200

-- Lines of history vim has to remember.
vim.o.history = 500

-- How many undos.
vim.o.undolevels = 1000

-- Enable persistent undo.
vim.o.undofile = true

-- - Shada ------------------------------------------------------------------ --

-- Tell vim to remember certain things when we exit:
-- !    : Save VARIABLE_IN_ALL_CAPS.
-- '100 : Number of files remembered.
-- <100 : Save up to 50 lines for each register.
-- s50  : Maximum size in kiB.
-- h    : Disable effect of highlight search when loading the shada file.
-- r... : No marks will be stored for any of these paths.
vim.o.shada = [[!,'100,<100,s50,h,r/tmp,r/run/media]]

-- - Sessions --------------------------------------------------------------- --

vim.o.sessionoptions = 'buffers,curdir,folds,tabpages,winsize'
vim.keymap.set('n', '<leader>ms', ':mksession!<cr>', { desc = '[M]ake [S]ession file' })

-- = Editing ================================================================ --

-- Default to LaTeX if all else fails.
vim.g.tex_flavor = 'latex'

-- - Keymaps ---------------------------------------------------------------- --

-- Two reflow options.
--
-- The need for this comes from this PR:
-- https://github.com/neovim/neovim/pull/19677. With it we gain access to LSP
-- based code formatting, but we loose the ability to reflow with vim's own
-- logic any comments in that code.
--
-- We could disable one or the other, but they're both useful. Fortunately
-- there's two ways to skin this one where one of them always bypasses the
-- formatprog and formatexpr, originally with the purpose of keeping the cursor
-- in the same text position.
--
-- So here's how it will go:
--
-- 1. Dumb reflow. It keeps working in comments, but wreaks havoc in code.
--
-- 2. LSP based code reflow or dumb reflow if no formatprog / formatexpr
--    is set. Will also not save cursor position.
vim.keymap.set('n', '<M-q>', 'gwip', { silent = true, desc = 'Reflow text' })
vim.keymap.set('v', '<M-q>', 'gw',   { silent = true, desc = 'Reflow text' })
vim.keymap.set('n', '<M-Q>', 'gqip', { silent = true, desc = 'Format with formatprog / formatexpr' })
vim.keymap.set('v', '<M-Q>', 'gq',   { silent = true, desc = 'Format with formatprog / formatexpr' })

-- Push lines up and down.
vim.keymap.set('n', '<M-j>', [[mz:m+<cr>`z]],               { silent = true, desc = 'Move line down' })
vim.keymap.set('n', '<M-k>', [[mz:m-2<cr>`z]],              { silent = true, desc = 'Move line up' })
vim.keymap.set('v', '<M-j>', [[:m'>+<cr>`<my`>mzgv`yo`z]],  { silent = true, desc = 'Move line down' })
vim.keymap.set('v', '<M-k>', [[:m'<-2<cr>`>my`<mzgv`yo`z]], { silent = true, desc = 'Move line up' })

-- Keep in visual-mode after indentation commands.
vim.keymap.set('v', '>', '>gv', { silent = true, desc = 'Indent selection' })
vim.keymap.set('v', '<', '<gv', { silent = true, desc = 'Unindent selection' })

-- - Completion ------------------------------------------------------------- --

-- Always show menu and preview and complete the longest common string.
vim.o.completeopt = 'menuone,preview,longest'

-- - White space ------------------------------------------------------------ --

-- Place caret at the beginning of the tab range.
vim.o.list = true

-- Default white space visualizer.
vim.o.listchars = 'tab:  '

-- Toggle white space visibility.
local function toggle_white_space()
  if vim.wo.listchars == 'tab:  ' or vim.wo.listchars == '' then
    vim.wo.listchars = 'tab:› ,space:·,eol:¶'
  else
    vim.wo.listchars = 'tab:  '
  end
end

vim.keymap.set({ 'n', 'i' }, '<F1>',
  toggle_white_space, { desc = 'Toggle visible white space' })

vim.api.nvim_create_user_command('ToggleWhiteSpace',
  toggle_white_space, { desc = 'Toggle visible white space' })

-- Delete trailing white space in the whole file.
local function trim_white_space()
  vim.cmd([[
    execute "normal mz"
    %s/\s\+$//ge
    execute "normal `z"
  ]])
end

-- Toggle whether automatic trimming is enabled for the current buffer.
local function toggle_trim_white_space()
  if vim.b['trim_white_space'] == nil or vim.b['trim_white_space'] then
    vim.b['trim_white_space'] = false
    print('white space trimming on save disabled')
  else
    vim.b['trim_white_space'] = true
    print('white space trimming on save enabled')
  end
end

-- Delete trailing white space on buffer write.
vim.api.nvim_create_autocmd({ 'BufWrite' }, {
  callback = function()
    if vim.b['trim_white_space'] == nil or vim.b['trim_white_space'] then
      trim_white_space()
    end
  end
})

vim.api.nvim_create_user_command('TrimWhiteSpace',
  trim_white_space, { desc = 'Trim trailing white space' })
vim.api.nvim_create_user_command('ToggleAutoTrimWhiteSpace',
  toggle_trim_white_space,
  { desc = 'Toggle trailing white space trimming on save' })

-- - Copyright notices ------------------------------------------------------ --

-- Update copyright notices with matching the pattern:
--   '<header> YYYY[-YYYY] <author> <<email>>'
local function update_copyright(header, author, email)
  local cursor_pos = vim.api.nvim_win_get_cursor(0)
  vim.cmd('silent exe "g/'
    .. header .. ' '
    .. [[\\(\\(".strftime("%Y")."\\)\\@!\\&[0-9]\\{4\\}\\)]]
    .. [[\\(\\(-".strftime("%Y")."\\)\\@!\\&-[0-9]\\{4\\}\\)\\?]]
    .. ' ' .. author .. ' <' .. email .. '>'
    .. '/s/'
    .. [[\\([0-9]\\{4\\}\\)\\(-[0-9]\\{4\\}\\)\\?/\\1-".strftime("%Y")]])
  vim.api.nvim_win_set_cursor(0, cursor_pos)
end

-- Toggle whether copyright notices should be updated for the current buffer.
local function toggle_update_copyright()
  if vim.b['update_copyright'] == nil or vim.b['update_copyright'] then
    vim.b['update_copyright'] = false
    print('copyright update on save disabled')
  else
    vim.b['update_copyright'] = true
    print('copyright update on save enabled')
  end
end

-- Update copyrights on save.
vim.api.nvim_create_autocmd({ 'BufWritePre' }, {
  callback = function()
    if (vim.b['update_copyright'] == nil or vim.b['update_copyright']) and
        vim.bo.modified then
      update_copyright('Copyright (c)', vim.g.author, vim.g.email)
    end
  end,
})

vim.api.nvim_create_user_command('ToggleAutoUpdateCopyright',
  toggle_update_copyright,
  { desc = 'Toggle copyright update on save' })

-- - Formatting ------------------------------------------------------------- --

-- Number detection (for <C-a> and <C-x>).
vim.o.nrformats = 'bin,hex'

-- Some mild, but helpful automatic formatting features.
vim.o.formatoptions = 'tcqrnj'

-- Expand the types of lists that can be formatted to include '*'s and '-'s.
vim.o.formatlistpat = [[^\s*\(\d\+[\]:.)}\t ]\|[*-]\+\)\s*]]

-- Ruler is set on the column after 'textwidth'.
vim.o.textwidth = 80
vim.o.colorcolumn = '+1'

-- Use a single space between sentences.
vim.o.joinspaces = false

-- - Indentation and tabs --------------------------------------------------- --

-- Automatic indentation.
vim.o.autoindent = true
-- C-like indentation.
vim.o.cindent = true
-- Don't expand tabs into spaces.
vim.o.expandtab = false
-- Tab size.
vim.o.tabstop = 8
-- Backspace removes this number of spaces (0: disabled); useful with expand tab.
vim.o.softtabstop = 0
-- Number of spaces to use for auto indenting.
vim.o.shiftwidth = 8
-- Use multiple of 'shiftwidth' when indenting with '<' and '>'.
vim.o.shiftround = true
-- Insert tabs on the start of a line according to 'shiftwidth', not 'tabstop'.
vim.o.smarttab = true
-- Automatic indentation rules.
vim.o.cinoptions = ''
    .. '>s,e0,n0,f0,{0,}0,^0,L-1,:0,=s,l1,b0,g0.5s,hs,'
    .. 'N-s,E-s,ps,ts,is,+s,c3,C0,/0,(0,u0,U0,w0,Ws,k0,m0,M0,j1,J1,)20,*70,#0'

-- - Spell checking --------------------------------------------------------- --

-- Spell check by default.
vim.o.spell = true

-- Toggle spell checking.
vim.keymap.set('n', '<leader>ss', ':setlocal spell! spell?<cr>', { silent = true, desc = 'Toggle spell checking' })

-- Dictionaries.
vim.o.spelllang = 'pt,en_gb'
vim.o.spellfile = vim.fn.stdpath('state') .. '/spell/pt.utf-8.add,'
    .. vim.fn.stdpath('state') .. '/spell/engb.utf-8.add'

-- Toggle spell language and file between the given language and the default.
-- Personal dictionary files must not be separated by regions, but we want to
-- separate by regions, so we take a 2nd argument as an easy way to specify a
-- matching dictionary file (without underscores basically).
local function toggle_spelllang(lang, dict)
  if vim.bo.spelllang == lang then
    vim.bo.spelllang = 'pt,en_gb'
    vim.bo.spellfile = ''
        .. vim.fn.stdpath('state') .. '/spell/pt.utf-8.add,'
        .. vim.fn.stdpath('state') .. '/spell/engb.utf-8.add'
  else
    vim.bo.spelllang = lang
    vim.bo.spellfile = vim.fn.stdpath('state') .. '/spell/' .. dict .. '.utf-8.add'
  end
  print(vim.bo.spelllang)
end

vim.keymap.set('n', '<leader>sus', function()
  toggle_spelllang('en_us', 'enus')
end, { silent = true, desc = 'Toggle spell checking' })
vim.keymap.set('n', '<leader>sgb', function()
  toggle_spelllang('en_gb', 'engb')
end, { silent = true, desc = 'Toggle spell checking' })
vim.keymap.set('n', '<leader>spt', function()
  toggle_spelllang('pt', 'pt')
end, { silent = true, desc = 'Toggle spell checking' })

-- - Folding ---------------------------------------------------------------- --

-- By far the most useful and versatile mode.
vim.o.foldmethod = 'manual'
-- Show thin column in gutter.
vim.o.foldcolumn = '1'

-- = Navigation ============================================================= --

-- Disable (back)space caret movement.
vim.keymap.set({ 'n', 'v' }, '<space>',     '<nop>', { silent = true })
vim.keymap.set({ 'n', 'v' }, '<backspace>', '<nop>', { silent = true })

-- Automatically convert j/k in gj/gk on wrapped lines.
vim.keymap.set('n', 'k', 'v:count == 0 ? "gk" : "k"', { expr = true, silent = true })
vim.keymap.set('n', 'j', 'v:count == 0 ? "gj" : "j"', { expr = true, silent = true })

-- - Scrolling -------------------------------------------------------------- --

-- Minimum lines around cursor.
vim.o.scrolloff = 10
-- In case wrapping is disabled.
vim.o.sidescroll = 1
vim.o.sidescrolloff = 10

-- Allow arrow keys to navigate through end of lines.
vim.o.whichwrap = '<,>,[,]'

-- - Buffer/Tab switching --------------------------------------------------- --

-- Buffers.
vim.keymap.set('n', '<leader>b', ':enew<cr>',      { silent = true, desc = 'New buffer' })
vim.keymap.set('n', '<leader>B', ':bdelete<cr>',   { silent = true, desc = 'Close buffer' })
vim.keymap.set('n', ']b',        ':bnext<cr>',     { silent = true, desc = 'Next buffer' })
vim.keymap.set('n', '[b',        ':bprevious<cr>', { silent = true, desc = 'Previous buffer' })

-- Tabs.
vim.keymap.set('n', '<leader>t', ':tabnew<cr>',      { silent = true, desc = 'New tab' })
vim.keymap.set('n', '<leader>T', ':tabclose<cr>',    { silent = true, desc = 'Close tab' })
vim.keymap.set('n', ']t',        ':tabnext<cr>',     { silent = true, desc = 'Next tab' })
vim.keymap.set('n', '[t',        ':tabprevious<cr>', { silent = true, desc = 'Previous tab' })

-- Windows.
vim.keymap.set('n', '<C-j>', '<C-W>j', { silent = true, desc = 'Move to window below' })
vim.keymap.set('n', '<C-k>', '<C-W>k', { silent = true, desc = 'Move to window above' })
vim.keymap.set('n', '<C-h>', '<C-W>h', { silent = true, desc = 'Move to window to the left' })
vim.keymap.set('n', '<C-l>', '<C-W>l', { silent = true, desc = 'Move to window to the right' })

-- Quickfix.
vim.keymap.set('n', '<leader>q', ':copen<cr>',     { silent = true, desc = 'Open quickfix window' })
vim.keymap.set('n', '<leader>Q', ':cclose<cr>',    { silent = true, desc = 'Open quickfix window' })
vim.keymap.set('n', ']q',        ':cnext<cr>',     { silent = true, desc = 'Jump to the next quickfix line' })
vim.keymap.set('n', '[q',        ':cprevious<cr>', { silent = true, desc = 'Jump to the previous quickfix line' })
vim.keymap.set('n', '[Q',        ':cfirst<cr>',    { silent = true, desc = 'Jump to the first quickfix line' })
vim.keymap.set('n', ']Q',        ':clast<cr>',     { silent = true, desc = 'Jump to the last quickfix line' })

-- Location list.
vim.keymap.set('n', '<leader>r', ':lopen<cr>',     { silent = true, desc = 'Open location list window' })
vim.keymap.set('n', '<leader>R', ':lclose<cr>',    { silent = true, desc = 'Open location list window' })
vim.keymap.set('n', ']r',        ':lnext<cr>',     { silent = true, desc = 'Jump to the next location list line' })
vim.keymap.set('n', '[r',        ':lprevious<cr>', { silent = true, desc = 'Jump to the previous location list line' })
vim.keymap.set('n', '[R',        ':lfirst<cr>',    { silent = true, desc = 'Jump to the first location list line' })
vim.keymap.set('n', ']R',        ':llast<cr>',     { silent = true, desc = 'Jump to the last location list line' })

-- = Searching ============================================================== --

vim.keymap.set('n', '<leader><cr>', ':nohlsearch<cr>', { silent = true, desc = 'Turn off search highlights' })

-- Show search matches as you type.
vim.o.incsearch = true
-- Case insensitive by default; sensitive if capitals are used.
vim.o.ignorecase = true
vim.o.smartcase = true
-- Wrap through the end of the file while searching.
vim.o.wrapscan = true

-- Use ripgrep over grep.
if vim.fn.executable('rg') == 1 then
  vim.o.grepprg = 'rg --vimgrep --hidden --ignore-file ~/.gitignore --ignore-file ~/.rgignore'
  vim.o.grepformat = '%f:%l:%c:%m'
else
  print('INFO: ripgrep not found, using default grep')
end
