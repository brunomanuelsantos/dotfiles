-- Copyright (c) 2023 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- - Preamble --------------------------------------------------------------- --
-- From `:h luasnip.txt`'s introduction.
local ls = require("luasnip")
local s = ls.snippet
local sn = ls.snippet_node
-- local isn = ls.indent_snippet_node
-- local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local c = ls.choice_node
-- local d = ls.dynamic_node
-- local r = ls.restore_node
-- local events = require("luasnip.util.events")
-- local ai = require("luasnip.nodes.absolute_indexer")
-- local extras = require("luasnip.extras")
-- local l = extras.lambda
-- local rep = extras.rep
-- local p = extras.partial
-- local m = extras.match
-- local n = extras.nonempty
-- local dl = extras.dynamic_lambda
local fmt = require("luasnip.extras.fmt").fmt
-- local fmta = require("luasnip.extras.fmt").fmta
-- local conds = require("luasnip.extras.expand_conditions")
-- local postfix = require("luasnip.extras.postfix").postfix
-- local types = require("luasnip.util.types")
-- local parse = require("luasnip.util.parser").parse_snippet
-- local ms = ls.multi_snippet

-- - My modules ------------------------------------------------------------- --

local fn = require('my-plugins.snippet_helpers')

-- - Snippets --------------------------------------------------------------- --

-- Get all of C's snippets as well.
ls.filetype_extend('cpp', { 'c' })

ls.add_snippets('cpp', {

  -- for (:) {}
  s('for',
    fmt('for ({} : {}) {{\n{}{}\n}}', {
      c(1, {
        i(nil, 'auto &elem'),
        sn(nil, fmt('{}{}', {
          i(1, 'auto &'),
          i(2, 'elem'),
        })),
      }),
      i(2, 'container'),
      f(function() return fn.indent(1) end), i(3),
    })),

  -- try {} catch {}
  s('try',
    fmt('try {{\n{}{}\n}}', {
      f(function() return fn.indent(1) end), i(1),
    })),
  s('catch',
    fmt('catch ({} &{}) {{\n{}{}\n}}', {
      i(1, 'std::runtime_error'),
      i(2),
      f(function() return fn.indent(1) end), i(3),
    })),
})
