-- Copyright (c) 2023 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- - Preamble --------------------------------------------------------------- --
-- From `:h luasnip.txt`'s introduction.
local ls = require("luasnip")
local s = ls.snippet
local sn = ls.snippet_node
-- local isn = ls.indent_snippet_node
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local c = ls.choice_node
local d = ls.dynamic_node
-- local r = ls.restore_node
-- local events = require("luasnip.util.events")
-- local ai = require("luasnip.nodes.absolute_indexer")
-- local extras = require("luasnip.extras")
-- local l = extras.lambda
-- local rep = extras.rep
-- local p = extras.partial
-- local m = extras.match
-- local n = extras.nonempty
-- local dl = extras.dynamic_lambda
local fmt = require("luasnip.extras.fmt").fmt
-- local fmta = require("luasnip.extras.fmt").fmta
-- local conds = require("luasnip.extras.expand_conditions")
-- local postfix = require("luasnip.extras.postfix").postfix
-- local types = require("luasnip.util.types")
-- local parse = require("luasnip.util.parser").parse_snippet
-- local ms = ls.multi_snippet

-- - My modules ------------------------------------------------------------- --

local fn = require('my-plugins.snippet_helpers')

-- - Snippets --------------------------------------------------------------- --

ls.add_snippets('all', {

  -- /* - Separator ----------- */
  s({
      trig = '^sep(.)',
      regTrig = true,
      hidden = true,
    },
    fmt('{}{}{}\n{}', {
      d(1, function(_, parent)
        local filler = parent.captures[1]
        local start = fn.comment_delimeters().start
        return sn(nil, { t(start .. ' ' .. filler .. ' ') })
      end),
      i(2, 'Section'),
      f(function(args, parent)
        local filler = parent.captures[1]
        local finish = fn.comment_delimeters().finish
        local lhs_len = args[1][1]:len() + args[2][1]:len()
        local pad = vim.bo.textwidth - lhs_len - 2 - finish:len()
        return ' ' .. string.rep(filler, pad) .. ' ' .. finish
      end, { 1, 2 }),
      i(0)
    })),

  -- Copyright ...
  s({
      trig = 'c)',
      hidden = true,
    },
    fmt('Copyright (c) {} {} <{}>', {
      f(function()
        return vim.fn.strftime('%Y')
      end),
      t(vim.g.author),
      t(vim.g.email),
    })),

  -- SPDX-License-Identifier: ...
  s({
      trig = 'spdx',
      hidden = true,
    },
    fmt('SPDX-License-Identifier: {}{}', {
      c(1, {
        i(nil, 'license'),
        t('GPL-3.0-or-later'),
        t('GPL-3.0-only'),
        t('GPL-2.0-or-later'),
        t('GPL-2.0-only'),
        t('BSD-3-Clause'),
        t('MIT'),
        t('CC-BY-SA-4.0'),
      }),
      c(2, {
        t(''),
        sn(nil, {
          t(' WITH '),
          i(1, 'exceptions'),
        }),
      }),
    })),
})
