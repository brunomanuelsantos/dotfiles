-- Copyright (c) 2023 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- - Preamble --------------------------------------------------------------- --
-- From `:h luasnip.txt`'s introduction.
local ls = require("luasnip")
local s = ls.snippet
local sn = ls.snippet_node
-- local isn = ls.indent_snippet_node
-- local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local c = ls.choice_node
local d = ls.dynamic_node
-- local r = ls.restore_node
-- local events = require("luasnip.util.events")
-- local ai = require("luasnip.nodes.absolute_indexer")
local extras = require("luasnip.extras")
-- local l = extras.lambda
local rep = extras.rep
-- local p = extras.partial
-- local m = extras.match
-- local n = extras.nonempty
-- local dl = extras.dynamic_lambda
local fmt = require("luasnip.extras.fmt").fmt
-- local fmta = require("luasnip.extras.fmt").fmta
-- local conds = require("luasnip.extras.expand_conditions")
-- local postfix = require("luasnip.extras.postfix").postfix
-- local types = require("luasnip.util.types")
-- local parse = require("luasnip.util.parser").parse_snippet
-- local ms = ls.multi_snippet

-- - My modules ------------------------------------------------------------- --

local fn = require('my-plugins.snippet_helpers')

-- - Snippets --------------------------------------------------------------- --
--
-- Note these must be relevant for C++ as well, so some will need to check the
-- current `filetype` to do the right thing.

ls.add_snippets('c', {

  -- #include "header.h"
  s('#include',
    fmt('#include {}', {
      c(1, {
        sn(nil, fmt('"{}"', {
          d(1, function()
            local bufname = vim.fn.bufname():gsub('.*/', ''):match('(.*)%.c')
            bufname = (bufname or 'local') .. '.h'
            return sn(nil, { i(1, bufname) })
          end)
        })),
        sn(nil, fmt('<{}>', { i(1, 'system.h') })),
      })
    })),

  -- #ifndef GUARD #define GUARD #endif
  s('#guard',
    fmt('#ifndef {}\n#define {}\n\n{}\n\n#endif', {
      sn(1, {
        c(1, {
          -- Full path.
          d(nil, function()
            local guard = vim.fn.bufname():gsub('^src/', '')
            guard = guard:upper():gsub('[^A-Z0-9_]+', '_')
            return sn(nil, { i(1, guard) })
          end),

          -- Full path + random suffix.
          sn(nil, {
            d(1, function()
              local guard = vim.fn.bufname():gsub('^src/', '')
              guard = guard:upper():gsub('[^A-Z0-9_]+', '_')
              return sn(nil, { i(1, guard) })
            end),
            f(function() return '_' .. fn.rand_alphanum_str(8):upper() end, {}),
          }),

          -- Reserved namespace + full path + random suffix.
          sn(nil, {
            d(1, function()
              local guard = vim.fn.bufname():gsub('^src/', '')
              guard = guard:upper():gsub('[^A-Z0-9_]+', '_')
              return sn(nil, { i(1, '_' .. guard) })
            end),
            f(function() return '_' .. fn.rand_alphanum_str(8):upper() end, {}),
          }),
        }),
      }),
      rep(1),
      i(0),
    })),

  -- if () {} else if () {} else { }
  s('if',
    fmt('if ({}) {{\n{}{}\n}}', {
      i(1, 'condition'),
      f(function() return fn.indent(1) end), i(2),
    })),
  s('elif',
    fmt('else if ({}) {{\n{}{}\n}}', {
      i(1, 'condition'),
      f(function() return fn.indent(1) end), i(2),
    })),
  s('else',
    fmt('else {{\n{}{}\n}}', {
      f(function() return fn.indent(1) end), i(1),
    })),

  -- a ? b : c
  s('ternary',
    fmt('{} ? {} : {};', {
      i(1, 'condition'),
      i(2, 'then'),
      i(3, 'else'),
    })),

  -- for (;;) {}
  s('for',
    fmt('for ({}; {}; {}) {{\n{}{}\n}}', {
      c(1, {
        i(nil, 'size_t i = 0'),
        sn(nil, fmt('{}{} = {}', {
          i(1, 'size_t '),
          i(2, 'i'),
          i(3, '0'),
        })),
      }),
      d(2, function(arg)
        local var_name = arg[1][1]:match('(%w+)%s*=') or ''
        return sn(nil, c(1, {
          i(nil, var_name .. ' < limit'),
          sn(nil, fmt('{} {} {}', {
            i(1, var_name),
            i(2, '<'),
            i(3, 'limit'),
          })),
        }))
      end, { 1 }),
      d(3, function(arg)
        local var_name = arg[1][1]:match('(%w+)%s*=') or ''
        return sn(nil, c(1, {
          d(1, function()
            print(vim.bo.filetype)
            if 'cpp' == vim.bo.filetype then
              return sn(nil, i(1, '++' .. var_name))
            else
              return sn(nil, i(1, var_name .. '++'))
            end
          end),
          sn(nil, fmt('{}{}{}', {
            d(1, function()
              print(vim.bo.filetype)
              if 'cpp' == vim.bo.filetype then
                return sn(nil, i(1, '++'))
              else
                return sn(nil, i(1, var_name))
              end
            end),
            d(2, function()
              if 'cpp' == vim.bo.filetype then
                return sn(nil, i(1, var_name))
              else
                return sn(nil, i(1, '++'))
              end
            end),
            i(3),
          })),
        }))
      end, { 1 }),
      f(function() return fn.indent(1) end), i(4),
    })),

  -- while () {}
  s('while',
    fmt('while ({}) {{\n{}{}\n}}', {
      i(1, 'condition'),
      f(function() return fn.indent(1) end), i(2),
    })
  ),

  s('do',
    fmt('do {{\n{}{}\n}} while ({});', {
      f(function() return fn.indent(1) end), i(2),
      i(1, 'condition'),
    })
  ),

  -- switch () {}
  s('switch',
    fmt('switch ({}) {{\n{}{}\n}}', {
      i(1, 'expression'),
      f(function() return fn.indent(1) end), i(2),
    })
  ),

  -- int main() { return 0; }
  s('main',
    fmt('int main(int argc, char *argv[])\n{{\n{}\n{}\n\n{}{}\n\n{}\n}}', {
      f(function() return fn.indent(1) .. '(void) argc;' end),
      f(function() return fn.indent(1) .. '(void) argv;' end),
      f(function() return fn.indent(1) end), i(0),
      f(function() return fn.indent(1) .. 'return 0;' end),
    })),
})
