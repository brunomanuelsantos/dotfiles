-- Copyright (c) 2019-2023 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
-- SPDX-License-Identifier: GPL-3.0-or-later

vim.wo.spell = false

vim.b.trim_white_space = false
