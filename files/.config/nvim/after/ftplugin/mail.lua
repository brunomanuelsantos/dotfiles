-- Copyright (c) 2019-2023 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
-- SPDX-License-Identifier: GPL-3.0-or-later

vim.bo.textwidth = 72

-- Disable trailing white space trimming just in case there are patches inline.
vim.b.trim_white_space = false

-- When composing email from (neo)mutt...
--
-- Heuristic is: file is generated by (neo)mutt and it's modifiable. This should
-- be accurate in 99+% of the cases.
if vim.fn.matchstr('@%',[[^\(neo\){,1}mutt-]]) ~= nil and vim.bo.readonly == false then

  -- Remove last signature from the quote.
  pcall(function() vim.cmd([[%s /\n\(>\s*\n\)*> -- \(\n>\s\{,1}.*\)\+//]]) end)

end
