-- Copyright (c) 2023 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
-- SPDX-License-Identifier: GPL-3.0-or-later

vim.wo.spell = false

vim.keymap.set('n', '<F1>', ':ToggleWhiteSpace<cr>',
  { buffer = true, desc = 'Toggle visible white space' })
