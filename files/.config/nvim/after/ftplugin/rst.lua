-- Copyright (c) 2019-2023 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
-- SPDX-License-Identifier: GPL-3.0-or-later

vim.bo.expandtab = true
vim.bo.tabstop = 3
vim.bo.shiftwidth = 3
