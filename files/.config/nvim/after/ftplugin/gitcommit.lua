-- Copyright (c) 2019-2024 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
-- SPDX-License-Identifier: GPL-3.0-or-later

vim.o.spell = true
vim.bo.textwidth = 72
