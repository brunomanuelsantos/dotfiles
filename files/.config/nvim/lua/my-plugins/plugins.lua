-- Copyright (c) 2016-2025 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- = Plugins ================================================================ --

local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'

if not vim.loop.fs_stat(lazypath) then
  vim.fn.system {
    'git',
    'clone',
    '--filter=blob:none',
    'https://github.com/folke/lazy.nvim.git',
    '--branch=stable',
    lazypath,
  }
end

vim.opt.rtp:prepend(lazypath)

-- - Install plugins -------------------------------------------------------- --

local pluginspec = {

-- - Lest we forget --------------------------------------------------------- --

  -- Tree sitter parser support.
  {
    'nvim-treesitter/nvim-treesitter',
    dependencies = {
      'nvim-treesitter/nvim-treesitter-textobjects',
      'nvim-treesitter/playground',
    },
    config = function()
      pcall(require('nvim-treesitter.install').update { with_sync = true })
    end,
  },

  {
    'lambdalisue/suda.vim',
    config = function()
      vim.keymap.set('c', 'w!!', 'SudaWrite', { desc = 'sudo write' })
      vim.keymap.set('c', 'e!!', 'SudaRead', { desc = 'sudo write' })
    end,
  },

-- - Swag ------------------------------------------------------------------- --

  -- Theme.
  {
    'RRethy/nvim-base16',
    priority = 1000,
    config = function()
      vim.cmd.colorscheme 'base16-monokai'
    end,
  },

  -- Git gutter symbols.
  {
    'lewis6991/gitsigns.nvim',
    opts = {
      signs = {
        add          = { text = '│' },
        change       = { text = '│' },
        delete       = { text = '_' },
        topdelete    = { text = '‾' },
        changedelete = { text = '~' },
        untracked    = { text = '┆' },
      },
      signs_staged = {
        add          = { text = '│' },
        change       = { text = '│' },
        delete       = { text = '_' },
        topdelete    = { text = '‾' },
        changedelete = { text = '~' },
        untracked    = { text = '┆' },
      },
    },
  },

  -- Status line.
  {
    'nvim-lualine/lualine.nvim',
    dependencies = { 'nvim-tree/nvim-web-devicons' },
    opts = {
      options = {
        component_separators = '|',
        section_separators = '',
      },
      sections = {
        lualine_a = {
          {
            -- Customize component to indicate visual multi mode.
            'mode',
            fmt = function(mode)
              return vim.b['visual_multi'] and mode .. ' 󰗘  MULTI' or mode
            end,
            color = { gui = 'bold' },
          },
        },
        lualine_b = {
          'branch',
          'diagnostics',
          {
            -- Custom component to show visual multi's info line.
            function()
              local result = vim.fn["VMInfos"]()
              local current = result.current
              local total = result.total
              local patterns = result.patterns
              return " " .. patterns[1] .. " " .. current .. "/" .. total
            end
          }
        },
        lualine_c = { { 'filename', path = 1, symbols = { modified = " ●", readonly = " " } } },
        lualine_x = { 'encoding', 'fileformat', { 'filetype', colored = false } },
        lualine_y = { 'progress' },
        lualine_z = { 'location' },
      },
      tabline = {
        lualine_a = { 'windows' },
        lualine_b = {},
        lualine_c = {},
        lualine_x = {},
        lualine_y = {},
        lualine_z = { 'tabs' },
      },
      winbar = {},
      inactive_winbar = {},
      extensions = {
        'man',
        'quickfix',
        'mundo',
        'nvim-tree',
        'fugitive',
        'lazy',
      },
    },
  },

  -- Prompts
  { 'stevearc/dressing.nvim', opts = {} },
  {
    'nvim-telescope/telescope-ui-select.nvim',
    dependencies = { 'nvim-telescope/telescope.nvim' },
  },

-- - Dev tools -------------------------------------------------------------- --

  -- Git plugins
  'tpope/vim-fugitive',
  'tpope/vim-rhubarb',
  'shumphrey/fugitive-gitlab.vim',

  'rhysd/conflict-marker.vim',

-- - Search and destroy ----------------------------------------------------- --

  -- File browser pane.
  {
    'nvim-tree/nvim-tree.lua',
    opts = {
      sort_by = 'case_sensitive',
      git = {
        enable = false
      },
      filters = {
        dotfiles = true,
      },
      renderer = {
        icons = {
          show = {
            folder = false,
            file = false,
            git = false,
            folder_arrow = true,
            modified = false,
          },
          glyphs = {
            symlink = '',
          },
        },
      },
    },
  },

  -- Fuzzy search.
  {
    'nvim-telescope/telescope.nvim',
    version = '*',
    dependencies = {
      'nvim-lua/plenary.nvim',
      'nvim-tree/nvim-web-devicons',
    },
  },
  {
    'nvim-telescope/telescope-fzf-native.nvim',
    build = 'make',
    cond = function()
      return vim.fn.executable 'make' == 1
    end,
  },

  -- Better navigation motions (check configuration...)
  -- 'ggandor/leap.nvim',
  -- 'ggandor/flit.nvim',

-- - Editing ---------------------------------------------------------------- --

  -- Multi region edits.
  {
    'mg979/vim-visual-multi',
    init = function()
      -- Must be set before the plugin loads as these become permanent.
      vim.cmd([[let g:VM_maps = {}]])
      vim.cmd([[let g:VM_maps['Find Under']         = ';']])
      vim.cmd([[let g:VM_maps['Find Subword Under'] = ';']])

      -- Do not clobber lualine. We'll have lualine show us the current mode.
      vim.g.VM_set_statusline = 0
      vim.g.VM_silent_exit = 1

      vim.g.VM_highlight_matches = ''
      vim.g.VM_verbose_commands = 1
    end,
  },

  -- Hex mode.
  'fidian/hexmode',

  -- Autocompletion and snippet engines.
  {
    'hrsh7th/nvim-cmp',
    dependencies = {
      'hrsh7th/cmp-nvim-lsp',
      'L3MON4D3/LuaSnip',
      'saadparwaiz1/cmp_luasnip'
    },
  },

  -- Undo tree pane.
  'simnalamburt/vim-mundo',

  -- Manipulate enclosing characters.
  { 'kylechui/nvim-surround', version = '*', opts = {} },

  -- Upper/mixed/camel/snake/dash/dot casing
  'tpope/vim-abolish',

  -- No comments.
  { 'numToStr/Comment.nvim', opts = {} },

  -- Detect tabstop and shiftwidth automatically.
  {
    'tpope/vim-sleuth',
    init = function()
      -- Some file types don't work so well...
      vim.g.sleuth_rst_heuristics = 0
    end,
  },

-- - LSPs ------------------------------------------------------------------- --

  {
    'neovim/nvim-lspconfig',
    dependencies = {
      -- Automatically install LSPs
      'williamboman/mason.nvim',
      'williamboman/mason-lspconfig.nvim',

      -- LSP progress UI
      { 'j-hui/fidget.nvim', opts = {}, tag = 'legacy' },
    },
  },

  -- Neovim's lua API documentation, completion, etc.
  { 'folke/neodev.nvim', opts = {} },

-- - Local plugins ---------------------------------------------------------- --

  -- require 'plugins.dir.name',
  -- { import = 'plugins.dir' },
}

-- Install plugins.
require('lazy').setup(pluginspec, {
  performance = {
    rtp = {
      -- Add the 'site/' path to the runtime. This is where the spell
      -- dictionaries are installed. See
      -- https://github.com/folke/lazy.nvim/issues/64 for instance.
      paths = { vim.fn.stdpath('data') .. '/site' },
    }
  }
})

-- = Plugin configuration =================================================== --

-- - Setup tree sitter ------------------------------------------------------ --

require('nvim-treesitter.configs').setup {
  ensure_installed = 'all',
  highlight = { enable = true },
  -- Experimental...
  -- indent = { enable = true, disable = { 'c', 'cpp', 'python' } },
  incremental_selection = {
    enable = true,
    keymaps = {
      init_selection = '<c-space>',
      node_incremental = '<c-space>',
      scope_incremental = '<c-s>',
      node_decremental = '<M-space>',
    },
  },
  textobjects = {
    select = {
      enable = true,
      lookahead = true,
      keymaps = {
        ['aa'] = '@parameter.outer',
        ['ia'] = '@parameter.inner',
        ['af'] = '@function.outer',
        ['if'] = '@function.inner',
        ['ao'] = '@class.outer',
        ['io'] = '@class.inner',
        ['ac'] = '@comment.outer',
        ['ic'] = '@comment.inner',
      },
    },
    move = {
      enable = true,
      set_jumps = true,
      goto_next_start = {
        [']n'] = '@function.inner',
        [']m'] = '@function.outer',
        [']]'] = '@class.outer',
      },
      goto_previous_start = {
        ['[n'] = '@function.inner',
        ['[m'] = '@function.outer',
        ['[['] = '@class.outer',
      },
      goto_next_end = {
        [']M'] = '@function.outer',
        [']['] = '@class.outer',
      },
      goto_previous_end = {
        ['[M'] = '@function.outer',
        ['[]'] = '@class.outer',
      },
    },
    swap = {
      enable = true,
      swap_next = {
        ['<leader>a'] = '@parameter.inner',
      },
      swap_previous = {
        ['<leader>A'] = '@parameter.inner',
      },
    },
  },
}

-- - Dev tools -------------------------------------------------------------- --

require('gitsigns').setup {
  on_attach = function(bufnr)
    local gitsigns = require('gitsigns')

    local function map(mode, l, r, opts)
      opts = opts or {}
      opts.buffer = bufnr
      vim.keymap.set(mode, l, r, opts)
    end

    map('n', ']c', function()
      if vim.wo.diff then
        vim.cmd.normal({']c', bang = true})
      else
        gitsigns.nav_hunk('next')
      end
    end)

    map('n', '[c', function()
      if vim.wo.diff then
        vim.cmd.normal({'[c', bang = true})
      else
        gitsigns.nav_hunk('prev')
      end
    end)
  end
}

-- - Telescope configuration ------------------------------------------------ --

require('telescope').setup {
  defaults = {
    color_devicons = false,
    layout_strategy = 'horizontal',
    preview = true,
    border = true,
    borderchars = { '─', '│', '─', '│', '╭', '╮', '╯', '╰' },
    vimgrep_arguments = {
      "rg",
      "--color=never",
      "--hidden",
      "--no-heading",
      "--with-filename",
      "--line-number",
      "--column",
      "--smart-case",
      "--trim",
      "--ignore-file",
      "~/.gitignore",
      "--ignore-file",
      "~/.rgignore",
    }
  },
  pickers = {
    current_buffer_fuzzy_find = {},
    buffers = {},
    find_files = { hidden = true },
    help_tags = {},
    man_pages = { sections = { 'ALL' } },
    live_grep = {},
    grep_string = {},
    diagnostics = {},
    lsp_references = {},
    lsp_document_symbols = {},
    lsp_dynamic_workspace_symbols = {},
  },
}

require('telescope').load_extension('fzf')

vim.keymap.set('n', '<leader>/', require('telescope.builtin').current_buffer_fuzzy_find, { desc = '[/] Fuzzily search in current buffer' })
vim.keymap.set('n', '<leader><space>', require('telescope.builtin').buffers, { desc = '[ ] Find existing buffers' })
vim.keymap.set('n', '<leader>sf', require('telescope.builtin').find_files,   { desc = '[S]earch [F]iles' })
vim.keymap.set('n', '<leader>sh', require('telescope.builtin').help_tags,    { desc = '[S]earch [H]elp' })
vim.keymap.set('n', '<leader>sm', require('telescope.builtin').man_pages,    { desc = '[S]earch [M]an pages' })
vim.keymap.set('n', '<leader>sg', require('telescope.builtin').live_grep,    { desc = '[S]earch [G]rep interactively' })
vim.keymap.set('n', '<leader>sG', require('telescope.builtin').grep_string,  { desc = '[S]earch [G]rep string under cursor' })
vim.keymap.set('n', '<leader>sd', require('telescope.builtin').diagnostics,  { desc = '[S]earch [D]iagnostics' })
vim.keymap.set('n', '<leader>sq', require('telescope.builtin').quickfix,     { desc = '[S]earch [Q]uickfix' })

-- Make telescope's borders visible.
vim.api.nvim_set_hl(0, 'TelescopeBorder', { link = 'FloatBorder' })
vim.api.nvim_set_hl(0, 'TelescopePromptBorder', { link = 'FloatBorder' })

-- - File browser ----------------------------------------------------------- --

vim.keymap.set('n', '<F2>', ':NvimTreeToggle<cr>', { desc = 'Toggle file browser pane' })

-- - Undo tree -------------------------------------------------------------- --

vim.keymap.set('n', '<F3>', ':MundoToggle<cr>', { desc = 'Toggle undo tree pane' })

-- - Hex mode -------------------------------------------------------------- --

vim.keymap.set('n', '<F4>', ':Hexmode<cr>', { silent = true, desc = 'Toggle hex editing mode' })

-- - LSP settings ----------------------------------------------------------- --

-- Log file will grow indefinitely otherwise.
vim.lsp.set_log_level("off")

-- Additional setup for when an LSP connects to a particular buffer.
local on_attach = function(_, bufnr)
  local nmap = function(keys, func, desc)
    if desc then
      desc = 'LSP: ' .. desc
    end

    vim.keymap.set('n', keys, func, { buffer = bufnr, desc = desc })
  end

  nmap('<leader>rn', vim.lsp.buf.rename,      '[R]e[n]ame')
  nmap('<leader>ca', vim.lsp.buf.code_action, '[C]ode [A]ction')

  nmap('gd', vim.lsp.buf.definition,                      '[G]oto [D]efinition')
  nmap('gD', vim.lsp.buf.declaration,                     '[G]oto [D]eclaration')
  nmap('gI', vim.lsp.buf.implementation,                  '[G]oto [I]mplementation')
  nmap('gt', vim.lsp.buf.type_definition,                 '[G]oto [T]ype definition')
  nmap('gr', require('telescope.builtin').lsp_references, '[G]oto [R]eferences')

  nmap('<leader>ds', require('telescope.builtin').lsp_document_symbols,          '[D]ocument [S]ymbols')
  nmap('<leader>ws', require('telescope.builtin').lsp_dynamic_workspace_symbols, '[W]orkspace [S]ymbols')

  nmap('K',         vim.lsp.buf.hover,          'Hover Documentation')
  nmap('<leader>k', vim.lsp.buf.signature_help, 'Signature Documentation')

  nmap('<leader>wa', vim.lsp.buf.add_workspace_folder,    '[W]orkspace [A]dd Folder')
  nmap('<leader>wr', vim.lsp.buf.remove_workspace_folder, '[W]orkspace [R]emove Folder')
  nmap('<leader>wl', function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  end, '[W]orkspace [L]ist Folders')
end

-- LSP servers to install and their static / default settings.
local servers = {
  clangd = {},
  pylsp = {},
  lua_ls = {
    Lua = {
      workspace = { checkThirdParty = false },
      telemetry = { enable = false },
    },
  },
  bashls = {},
  cmake = {},
  yamlls = {},
  esbonio = {},
  marksman = {},
  dotls = {},
}

-- Adjust Python's LSP settings according to the environment.
--
-- If 'flake8' configuration is found, switch over, otherwise stick with
-- defaults.
local detect_flake8 = function()
  local files = { ".flake8", "setup.cfg", "tox.ini" }
  for _, file in ipairs(files) do
    local conf = io.open(file, "r")
    if conf ~= nil then
      for line in conf:lines() do
        if line == "[flake8]" then
          return true
        end
      end
    end
  end
  return false
end

if detect_flake8() then
  servers.pylsp = {
    pylsp = {
      configurationSources = { "flake8" },
      plugins = {
        flake8 = { enabled = true },
        mccabe = { enabled = false },
        pycodestyle = { enabled = false },
        pyflakes = { enabled = false },
      },
    },
  }
end

-- Install and configure servers.
require('mason').setup()

require('mason-lspconfig').setup {
  ensure_installed = vim.tbl_keys(servers),
}

-- Configure handlers.
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

require('mason-lspconfig').setup_handlers {
  function(server_name)
    require('lspconfig')[server_name].setup {
      capabilities = capabilities,
      on_attach = on_attach,
      settings = servers[server_name],
    }
  end,
}

-- Add borders to LSP handlers' floating windows.
vim.lsp.handlers['textDocument/hover'] = vim.lsp.with(
  vim.lsp.handlers.hover, { border = 'single' }
)
vim.lsp.handlers['textDocument/signatureHelp'] = vim.lsp.with(
  vim.lsp.handlers.signature_help, { border = 'single' }
)

-- - Autocompletion and snippet engines ------------------------------------- --

local luasnip = require 'luasnip'
luasnip.config.setup {
  history = true,
  update_events = 'TextChanged,TextChangedI',
  delete_check_events = 'TextChanged',
}

local cmp = require 'cmp'

cmp.setup {
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body)
    end,
  },
  mapping = cmp.mapping.preset.insert {
    ['<C-u>'] = cmp.mapping.scroll_docs(-4),
    ['<C-d>'] = cmp.mapping.scroll_docs(4),
    ['<C-space>'] = cmp.mapping.confirm {
      behavior = cmp.ConfirmBehavior.Replace,
      select = true,
    },
  },
  sources = {
    {
      -- Autocomplete from LSP, but _not_ all snippets. For languages we care
      -- enough, we will have our own snippets and we want no noise from other
      -- sources.
      name = "nvim_lsp",
      entry_filter = function(entry)
        if vim.bo.filetype == 'cpp' or vim.bo.filetype == 'cpp' then
          return require("cmp").lsp.CompletionItemKind.Snippet ~= entry:get_kind()
        else
          return true
        end
      end
    },
    { name = 'luasnip' },
  },
}

vim.keymap.set({ 'i', 's' }, '<C-space>', function()
  if luasnip.expand_or_jumpable() then
    luasnip.expand_or_jump()
  end
end, { silent = true, desc = 'Expand snippet / jump to next position' })
vim.keymap.set({ 'i', 's' }, '<C-j>', function()
  if luasnip.jumpable(1) then
    luasnip.jump(1)
  end
end, { silent = true, desc = 'Jump to next position' })
vim.keymap.set({ 'i', 's' }, '<C-k>', function()
  if luasnip.jumpable(-1) then
    luasnip.jump(-1)
  end
end, { silent = true, desc = 'Jump to previous position' })
vim.keymap.set({ 'i', 's' }, '<C-l>', function()
  if luasnip.choice_active() then
    luasnip.change_choice(1)
  end
end, { silent = true, desc = 'Cycle through snippet choice node options' })
