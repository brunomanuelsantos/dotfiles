-- Copyright (c) 2023 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
-- SPDX-License-Identifier: GPL-3.0-or-later

local helpers = {}

--- Return `n` levels worth of indentation white space.
-- The returned string respects the buffer `shiftwidth` and `expandtab`.
function helpers.indent(n)
  if vim.bo.expandtab then
    return string.rep(' ', n * vim.bo.shiftwidth)
  else
    return string.rep('\t', n)
  end
end

--- Get random alphanumerical string of length `len`.
-- Returned string uses the character range `[a-z0-9]`.
function helpers.rand_alphanum_str(len)
  local str = ''
  for _ = 1, len do
    local rand = math.random(0, 35)
    if rand < 10 then
      str = str .. string.char(rand + 48)
    else
      str = str .. string.char(rand - 10 + 97)
    end
  end
  return str
end

--- Returns comment delimeters for the current buffer.
--
-- The comment elements are returned as a 4-element table corresponding to the
-- start, middle and end comment delimiters together with the indentation of the
-- middle and end parts in number of spaces. For instance, for the C language,
-- the table is:
--
--    `{ start = '/*', middle = '*', finish = '*/', indent = 1 }`
--
-- And for Lua it would be:
--
--    `{ start = '--', middle = '--', finish = '--', indent = 0 }`
--
-- This first looks at the 'commentstring', if that ends with '%s', it uses the
-- prefix from that. Otherwise it parses 'comments' and prefers single character
-- comment markers if there are any.
--
-- This will return the first valid set it finds. If there is no valid set to be
-- found, the behaviour is undefined, but the table will always have all the
-- elements.
function helpers.comment_delimeters()

  local table = {}

  -- Try `commentstring` first.
  local start = vim.bo.commentstring:match('(.*)%s+%%s$')
  if start then
    table['start'] = start
    table['middle'] = start
    table['finish'] = start
    table['indent'] = 0
    return table
  end

  -- Having reached here, we need to parse `comments`.
  local elems = vim.split(vim.bo.comments, ',', { plain = true })

  for _, e in ipairs(elems) do
    local opt = vim.split(e, ':', { plain = true })
    local flag = opt[1]
    local text = opt[2]

    if flag:len() == 0 then
      table['start'] = text
      table['middle'] = text
      table['finish'] = text
      table['indent'] = 0
      return table

    elseif not flag:match('O') then
      if flag:match('s') then
        table['indent'] = flag:match('s(-?%d)')
        table['start'] = text
        if table.middle ~= nil and table.finish ~= nil then
          return table
        end
      elseif flag:match('m') then
        table['middle'] = text
        if table.start ~= nil and table.finish ~= nil then
          return table
        end
      elseif flag:match('e') then
        table['finish'] = text
        if table.start ~= nil and table.middle ~= nil then
          return table
        end
      end
    end
  end

  -- Fallback. Always return something, even if it's wrong.
  table['start'] = 'X'
  table['middle'] = 'X'
  table['finish'] = 'X'
  table['indent'] = 0

  return table
end

return helpers
