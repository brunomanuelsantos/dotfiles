# Copyright (c) 2016-2023 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later

# If not running interactively, don't do anything
# https://unix.stackexchange.com/questions/257571/why-does-bashrc-check-whether-the-current-shell-is-interactive
if [[ $- != *i* ]]; then
	return
fi


# = Behaviour ================================================================ #

# Disable legacy behaviour where scroll lock signals the terminal driver to halt
# sending. This is visible with a Ctrl-S locking the terminal until the turn off
# signal (Ctrl-Q) is sent.
stty -ixon

# Ignore lines starting with space in the history and remove duplicates
HISTCONTROL=ignorespace,erasedups

# Append to the history file, don't overwrite it
shopt -s histappend

# Set history file limits
HISTSIZE=10000
HISTFILESIZE=20000

# Enable pattern "**" in pathname expansion
shopt -s globstar


# = Colour scheme ============================================================ #

# Base16 Shell
if [[ -f ~/.config/base16-shell/scripts/base16-monokai.sh ]]; then
	source ~/.config/base16-shell/scripts/base16-monokai.sh
fi


# = Aliases & Functions ====================================================== #

# Separate file so that we can selectively use it from within other programs
# in non-interactive mode. Details linked at the top.
if [[ -f ~/.bash_aliases ]]; then
	source ~/.bash_aliases
fi


# = Prompt configuration ===================================================== #

# PS1 format
if [ "$(id -u)" != "0" ]; then
	# Normal user's prompt
	PS1="\[$BOLD$GREEN\]\u@\h\[$NC\] \[$BOLD$BLUE\]\w\[$NC\] \[$BOLD$BLUE\]\$ \[$NC\]"
else
	# Root's prompt
	PS1="\[$BOLD$RED\]\u@\h\[$NC\] \[$BOLD$BLUE\]\w\[$NC\] \[$BOLD$BLUE\]\$ \[$NC\]"
fi

# Trims working directory showing only the last x levels
export PROMPT_DIRTRIM=2
