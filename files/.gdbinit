# Copyright (c) 2016-2019 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
# SPDX-License-Identifier: GPL-3.0-or-later


# = Load dashboard if in terminal ============================================ #
#
# Very simple approach: check the parent process against a white list. This
# prevents the dashboard from being loaded if gdb is being executed from within
# a front end that is unlikely not to support it.
#
# In principal it only makes sense to white list the shell name itself and any
# helper command that may be used to launch gdb in the terminal.

python

import psutil

parent = psutil.Process().parent().name()

if parent in ['sh', 'bash', 'make']:
   print('\n>>> Detected non GUI parent \'{}\'. Loading dashboard.'.format(parent))
   gdb.execute('source ~/.gdb_dashboard')
else:
   print('\n>>> Detected unknown/GUI parent \'{}\'. Bypassing dashboard.'.format(parent))

end


# = General options ========================================================== #
#
# This is done after loading GDB dashboard so that these take precedence over
# the external dashboard ones.

set verbose off

set history save
set history remove-duplicates 20
set history filename ~/.gdb_history

set print pretty on
set print array off
set print array-indexes on

set python print-stack full


# = Load local .gdbinit ====================================================== #
#
# This poses security issues, so instead of using the auto load feature,
# explicitly ask the user about it and default to doing nothing.
#
# Asking the user is also important in case the local .gdbinit was created with
# dashboard in mind, e.g. with "dashboard -configuration .gdbinit".

python

from pathlib import Path
import os

# If in home directory, this is obviously not a 'local' .gdbinit file. Prevent
# potentially infinite successive inclusions of *this* very same file.
if os.path.normpath('.') != os.path.normpath('~'):
    if Path('.gdbinit').is_file():
        confirm = input('>>> Found local .gdbinit, load it? [y/N] ')
        print('')

        if confirm in ['y', 'Y', 'yes', 'Yes']:
            gdb.execute('source .gdbinit')

end
